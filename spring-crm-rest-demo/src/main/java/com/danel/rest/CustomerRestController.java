package com.danel.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.danel.entity.Customer;
import com.danel.service.CustomerService;

@RestController
@RequestMapping("/api")
public class CustomerRestController {
	
	//autowiring the CustomerService
	
	@Autowired
	private CustomerService customerService;
	
	//mapping for GET/customers 
	 @GetMapping("/customers")
	 public List<Customer> getCustomers(){
		 
		 return customerService.getCustomers()	;
		 
	 }
	 
	 
	 //Mapping for Get a single customer by id 
	 
	 @GetMapping("/customers/{customerId}")
	 public Customer getCustomer(@PathVariable int customerId) {
		 
		 Customer theCustomer = customerService.getCustomer(customerId);
		 
		 if(theCustomer == null) {
			 throw new CustomerNotFoundException("customer id not found -" + customerId);
		 }
		 return theCustomer;
}
	 
	 
	 //Mapping for adding a new customer
	 
	 @PostMapping("/customers")
	 public Customer addCustomer(@RequestBody Customer theCustomer) {
		 
		 theCustomer.setId(0);
		 customerService.saveCustomer(theCustomer);
		 return theCustomer;
		 
	 }
	 
	 
	 //Mapping Updating a customer
	 
	 @PutMapping("/customers")
	 public Customer updateCustomer(@RequestBody Customer theCustomer) {
		 
		 customerService.saveCustomer(theCustomer);
		 
		 return theCustomer;
		 
	 }
	 
	 //Mapping for deleting a customer
	 
	 @DeleteMapping("/customers/{customerId}")
	 public String deleteCustomer(@PathVariable int customerId) {
		 
		 Customer tempCustomer = customerService.getCustomer(customerId);
		 
		 if(tempCustomer == null) {
			 throw new CustomerNotFoundException("customer id not found -" + customerId);
		 }
		 
		 customerService.deleteCustomer(customerId);
		 
		 return "Deleted customer id -" + customerId;
		 
	 }
	 
	 
	 
		
}