package com.danel.testing;

import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import com.danel.dao.CustomerDAO;
import com.danel.dao.CustomerDAOImpl;
import com.danel.entity.Customer;
import com.danel.service.CustomerService;

@RunWith(SpringRunner.class)
public class CustomerDAOIntegrationTest {
	 
	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private CustomerDAO customer;
	
	@Test
	public void whengetCustomer_thenReturnCustomer() {
		
		//given
		Customer john = new Customer(1);
		entityManager.persist(john);
		entityManager.flush();
		
		//when
		
		Customer found = CustomerService.getCustomer(john.getId());
		
		//then
		
		assertThat(found.getId()).isEqualTo(john.getId);
	}
	
	

}
