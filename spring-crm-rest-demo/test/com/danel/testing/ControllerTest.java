package com.danel.testing;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.danel.entity.Customer;
import com.danel.rest.CustomerRestController;
import com.danel.service.CustomerService;

import ch.qos.logback.core.status.Status;

@RunWith(SpringRunner.class)
@WebMvcTest(CustomerRestController.class)
public class ControllerTest {
	
	@Autowired
	private Mock mvc;
	
	@MockBean
	private CustomerService service;
	
	@Test
	public void givenCustomers_whengetCustomers_thenReturnJsonArray() throws Exception{
		
		Customer john = new Customer("John");
		
		List<Customer> allCustomers = Arrays.asList(john);
		
		given (service.getCustomers()).willReturn(allCustomers);
		
		mvc.perform(get("api/customers"))
		   .contentType(MediaType.APPLICATION_JSON)
		   .andExpect(status().isOk())
		   .andExpect(jsonPath("$", hasSize(1)))
		    .andExpect(jsonPath("$[0].name", is(john.getLastName())));
	}
	
}
