package com.danel.testing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.danel.entity.Customer;

public class EntityTest {
	

    private Customer customer;
	protected void setUp()throws Exception{
		customer = new Customer("eric","erica","dd@gmail.com","0055253258");
	}
	
	
	public void testInstance() {
		assertNotNull("Not instacied",customer);
	}
	
	
	
	public void testGetParam() {
		assertEquals("U got a bad Fristname","eric",customer.getFirstName());
		assertEquals("got a bad Lastname","erica",customer.getLastName());
		assertEquals("got a bad email","dd@gmail",customer.getEmail());
		assertEquals("got a bad number","0055253258",customer.getPhone());		
	}
	
	public void testSetParam() {
		customer.setFirstName("eric");
		assertEquals("incorect firstName","eric",customer.getFirstName());
		customer.setLastName("erica");
		assertEquals("incorect lastName","erica",customer.getLastName());
		customer.setEmail("dd@gmail.com");
		assertEquals("incorect email","dd@gmail.com",customer.getEmail());		
	}
	
	@Test
	public void testEmail() {
		String address = "dd@gmail.com";
		Customer.isValidEmailAdress(address);	
	}
	
	@Test
	public void testBlank()
	  {
	    String address = "";
	    boolean expectedResult = false;
	    boolean actualResult = Customer.isValidEmailAdress(address);
	    assertTrue("email is blank",expectedResult) ; 
	  }
	
	@Test
	public void testAl()
	  {
	    String address = "al@foobar.com";
	    boolean expectedResult = true;
	    boolean actualResult = Customer.isValidEmailAdress(address);
	    assertTrue("email should be well formed",expectedResult); 	    
	  }
	
	public void testValidNumber() {
		String number = "0045789525";
		boolean result = customer.isValidNumber(number);
		assertTrue("Number is valid",result);	
	}


}
